all:
	$(MAKE) -C tests/ all

tests:
	$(MAKE) -C tests/ tests

clean:
	$(MAKE) -C tests/ clean

install:
	$(MAKE) -C tests/ install

installcheck:
	$(MAKE) -C tests/ installcheck

.PHONY: clean all tests install
