#include <stdio.h>

/*
 *  Copyright (C) 2002-2005 Novell/SUSE
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 2 of the
 *  License.
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	if (argc != 2){
		fprintf(stderr, "usage: %s <variable_to_check>\n", argv[0]);
		return 3;
	}

  /* OK if not found, not-OK if found */
  if (getenv(argv[1]) != NULL)
    {
      fprintf(stderr, "%s=%s\n", argv[1], getenv(argv[1]));
      return 1;
    }
  else 
    {  
      fprintf(stderr, "%s not set\n", argv[1]);
      return 0;
    }
}
