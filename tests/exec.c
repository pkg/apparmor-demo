#include <stdio.h>

/*
 *	Copyright (C) 2002-2005 Novell/SUSE
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation, version 2 of the
 *	License.
 */
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

int main(int argc, char *argv[])
{
	int i;
pid_t pid;

extern char **environ;

	if (argc < 2){
		fprintf(stderr, "usage: %s program [args] \n", argv[0]);
		return 1;
	}

	for (i = 1; i < argc; i++){
		printf("%s: child argv[%d]: %s\n", argv[0], i-1, argv[i]);
	}

	pid=fork();

	if (pid){	/* parent */
		int status;

		printf("%s: parent: waiting for child %ld\n", argv[0], (long) pid);

		while (wait(&status) != pid);

		printf("%s: parent: child wait status %d\n", argv[0], status);

		if (WIFEXITED(status)){
			printf("%s: parent: child exit status %d\n", argv[0], WEXITSTATUS(status));
			printf("PASS\n");
		}else{
			printf("FAILED, child did not exit normally\n");
		}
	}else{
		printf("%s: child: calling execve(\"%s\", ., .)\n", argv[0], argv[1]);

		(void)execve(argv[1], &argv[1], environ);
		printf("%s: child: execve: %s\n", argv[0], strerror (errno));

		/* exec failed, kill outselves to flag parent */

		if (kill(getpid(), SIGKILL) != 0) {
			printf("%s: child: kill(self, SIGKILL): %s\n", argv[0], strerror (errno));
		}

		if (raise(SIGKILL) != 0) {
			printf("%s: child: raise(SIGKILL): %s\n", argv[0], strerror (errno));
		}

		errno = 0;
		abort();
		printf("%s: child: abort(): %s\n", argv[0], strerror (errno));

		printf("%s: child: giving up and exiting %d\n", argv[0], 128 | SIGKILL);
		/* best we can do... */
		return 128 | SIGKILL;
	}

	return 0;
}
