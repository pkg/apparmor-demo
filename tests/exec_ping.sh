#! /bin/bash
#	Copyright (C) 2002-2005 Novell/SUSE
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License as
#	published by the Free Software Foundation, version 2 of the
#	License.

#=NAME exec
#=DESCRIPTION Runs exec() through ux, ix & px functionality

pwd=`dirname $0`
pwd=`cd $pwd ; /bin/pwd`

bin=$pwd

. $bin/prologue.inc $RETAIN

file=$(test -x /usr/bin/ping && echo /usr/bin/ping || echo /bin/ping) # handle merged-/usr systems
ok_libs="/{,usr/}lib/*/*\.so{,\.*}:mr"
ok_etc="/etc/*:r"
ok_setuid="cap:setuid"
ok_netraw="cap:net_raw"
ok_inet_dgram="network:inet%dgram"
ok_inet_raw="network:inet%raw"
ok_ix="ix"
ok_nameservice="abstraction:nameservice"

info exec ping with all caps it needs
genprofile $ok_libs $ok_etc $file:$ok_ix $ok_nameservice $ok_netraw $ok_inet_raw $ok_inet_dgram $ok_setuid
runchecktest "EXEC $file, inheriting rules" pass

info exec ping without suid capability
genprofile $ok_libs $ok_etc $file:$ok_ix $ok_inet_raw $ok_inet_dgram $ok_net_raw
runchecktest "EXEC $file, inheriting rules" fail

info exec ping uncontained
genprofile $file:ux
runchecktest "EXEC $file ux" pass
