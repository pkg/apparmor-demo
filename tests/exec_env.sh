#! /bin/bash
#	Copyright (C) 2002-2005 Novell/SUSE
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License as
#	published by the Free Software Foundation, version 2 of the
#	License.

#=NAME exec
#=DESCRIPTION Runs exec() through ux and Ux proving LD_LIBRARY_PATH scrubbing

pwd=`dirname $0`
pwd=`cd $pwd ; /bin/pwd`

bin=$pwd

. $bin/prologue.inc $RETAIN

file=$pwd/scrubbed
ok_ux_perm=ux
ok_Ux_perm=Ux

# exec: $file VAR
# return 0: VAR is not set
# return 1: VAR is set 
#
# ./exec_env $file VAR
# will FAIL if VAR is found
# will PASS if VAR is not found

info fork+exec $file using Ux and setting LD_LIBRARY_PATH, which should be removed by the scrubbing option
genprofile $file:Ux
export LD_LIBRARY_PATH=/tmp
runchecktest "EXEC Ux (scrubbing) using LD_LIBRARY_PATH" pass $file LD_LIBRARY_PATH
unset LD_LIBRARY_PATH

info fork+exec $file using Ux and not setting LD_LIBRARY_PATH, which should be not present
genprofile $file:Ux
runchecktest "EXEC ux (no scrub) using LD_LIBRARY_PATH" pass $file LD_LIBRARY_PATH


info fork+exec $file using ux and setting LD_LIBRARY_PATH, which should be found since no scrubbing applied
genprofile $file:ux
export LD_LIBRARY_PATH=/tmp
runchecktest "EXEC ux (no scrub) using LD_LIBRARY_PATH" fail $file LD_LIBRARY_PATH
unset LD_LIBRARY_PATH

info fork+exec $file using ux and not setting LD_LIBRARY_PATH, which should be not present
genprofile $file:ux
runchecktest "EXEC ux (no scrub) using LD_LIBRARY_PATH" pass $file LD_LIBRARY_PATH
